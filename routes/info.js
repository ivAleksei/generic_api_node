'use strict'

var express = require('express');
var router = express.Router();

var fs = require('fs');
var path = require('path');
var pkgAPI = require('../package.json');

var iconv = require('iconv-lite');

router.get('/api', function(req, res, next){
  res.json(pkgAPI);
})

module.exports = router;