'use strict'

var express = require('express');
var router = express.Router();

var fs = require('fs');
var path = require('path');

router.use('/sys_admin', require('./sys_admin/sys_admin'));


module.exports = router;