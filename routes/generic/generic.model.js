// ModuloModel.js

function ModuloModelDAO(model, populate) {
  this.model = model
  this.populate = populate
}

ModuloModelDAO.prototype.find = function (query, callback) {
  this.model.find(query).deepPopulate(this.populate).exec(callback)
}

ModuloModelDAO.prototype.findOne = function (_id, callback) {
  var query = {
    _id: _id
  }
  this.model.find(query).deepPopulate(this.populate).exec(callback)
}

ModuloModelDAO.prototype.aggregate = function (query, callback) {
  this.model.aggregate(query).exec(callback)
}

ModuloModelDAO.prototype.create = function (data, callback) {
  var model = new this.model(data)
  model.save(function (err, result) {
    callback(err, result)
  })
}

ModuloModelDAO.prototype.update = function (_id, data, callback) {
  var query = {
    _id: _id
  }
  this.model.update(query, data).exec(function (err, result) {
    callback(err, result)
  })
}

ModuloModelDAO.prototype.remove = function (_id, callback) {
  var query = {
    _id: _id
  }
  this.model.remove(query).exec(function (err, result) {
    callback(err, result)
  })
}

module.exports = function (mongoose, collection, objSchema, populate) {
  var DeepPopulate = require('mongoose-deep-populate')(mongoose)
  var SchemaClass = mongoose.Schema
  var SchemaCollection = new SchemaClass(objSchema)
  SchemaCollection.plugin(DeepPopulate)
  var Model = mongoose.model(collection, SchemaCollection)
  return new ModuloModelDAO(Model, populate)
}