var express = require('express');
var fs = require('fs');
var path = require('path');
var router = express();

var mongoose = require('../../db/mongoose');

// var auth = require('./auth');

var routerCreater = function (GenericController) {

  // GET ATIVOS
  router.get('/', GenericController.getActive.bind(GenericController));

  // GET TODOS (INCLUSIVE NÃO ATIVOS)
  router.get('/all', GenericController.getAll.bind(GenericController));

  // GET TODOS (INCLUSIVE NÃO ATIVOS)
  router.get('/q', GenericController.getByQuery.bind(GenericController));

  // GET UM PELO ID
  router.get('/:_id', GenericController.getById.bind(GenericController));

  // POST - CRIAÇÃO DE OBJETO
  router.post('/', GenericController.create.bind(GenericController));

  // POST - CRIAÇÃO DE ARRAY
  router.post('/arr', GenericController.createArray.bind(GenericController));

  // PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
  router.put('/:_id', GenericController.update.bind(GenericController));

  // DELETE - EXLUIR moduloS POR QUERY
  router.delete('/q', GenericController.removeQuery.bind(GenericController));

  // DELETE - EXLUIR moduloS POR ARRAY DE ID'S
  router.delete('/arr', GenericController.removeArray.bind(GenericController));

  // DELETE - EXLUIR moduloS POR UM ID
  router.delete('/:_id', GenericController.remove.bind(GenericController));

  return router;
};


module.exports = routerCreater;