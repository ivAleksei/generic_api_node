var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
  res.redirect('/api')
})

router.use('/api', require('./api'));

router.use('/info', require('./info'));



module.exports = router;