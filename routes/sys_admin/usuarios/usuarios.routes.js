// Usuarios.js
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('config');
var formidable = require('formidable');
var request = require('request');
var md5 = require('md5');

var mongoose = require('../../../db/mongoose');

var objSchema = {
  login: String,
  password: String,
  ativo: Boolean,
  _modulos: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'modulos'
  }],
  _paginas: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'paginas'
  }]
};

var Model = require('../../generic/generic.model')(mongoose, 'usuarios', objSchema, '_modulos _paginas');
var Controller = require('./usuarios.controller')(Model);

// LOGAR NO SISTEMA
router.post('/login', Controller.logIn.bind(Controller));

router.use('/', require('../../generic/generic.js')(Model));

module.exports = router;