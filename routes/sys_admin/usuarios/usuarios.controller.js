'use strict'

// UsuarioController.js

var md5 = require('md5');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var jwt = require('jwt-simple');
var config = require('config');

function UsuarioController(UsuarioModel) {
  this.model = UsuarioModel;
}

UsuarioController.prototype.getAll = function (req, res, next) {
  this.model.find({}, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

UsuarioController.prototype.getActive = function (req, res, next) {
  this.model.find({
    ativo: true
  }, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

UsuarioController.prototype.getByQuery = function (req, res, next) {
  this.model.find(req.query, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

UsuarioController.prototype.getById = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  model.findOne(_id, function (err, data) {
    if (err) return next(err);

    if (!data.length) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    }

    res.json(data[0]);
  });

};

UsuarioController.prototype.create = function (req, res, next) {

  var model = this.model;
  var obj = req.body;


  var persist = function (obj, files) {
    if (obj._id && obj._id != '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    if (obj._id == '')
      delete obj._id;

    model.create(obj, function (err, data) {
      if (err) {
        return next(err);
      }
      res.json(data);
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

UsuarioController.prototype.createArray = function (req, res, next) {

  var model = this.model;
  var obj = req.body;


  var persist = function (obj, files) {
    if (!obj.length) {
      var err = new Error('Array nao informado.');
      err.status = 400;
      return next(err);
    }

    var retorno = [];
    for (let b in obj) {
      if (obj[b]._id && obj[b]._id != '') {
        var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
        err.status = 400;
        return next(err);
      }

      if (obj[b]._id == '')
        delete obj[b]._id;

      model.create(obj[b], function (err, data) {
        if (err) {
          return next(err);
        }
        retorno.push(data);
        if (b == obj.length - 1)
          res.json(retorno);
      });
    }
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

UsuarioController.prototype.update = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  var obj = req.body;
  var persist = function (obj, files) {

    if (!_id || _id == '') {
      var err = new Error('Não foi possível atualizar. Identificador não informado.');
      err.status = 400;
      return next(err);
    }
    model.update(_id, obj, function (err, data) {
      if (err) return next(err);
      model.findOne(_id, function (err, data) {
        if (err) return next(err);
        let retorno = data[0];

        if (files) {
          for (var f in files) {
            var temp_path = files[f].path;
            var file_name = retorno[f].fakename;
            var type_folder = path.join(__dirname, '../files/' + f + '/')
            var new_location = path.join(type_folder, retorno._id + '/');

            var copy = function () {
              fs.rename(temp_path, path.join(new_location, file_name), function (err) {
                if (err) {
                  console.error(err);
                } else {
                  console.log("success!")
                }
              });
            }
            fs.mkdir(type_folder, function (err) {
              if (err) {}
              fs.mkdir(new_location, function (err) {
                if (err) {}
                copy();
              })
            })

          }
        }
        res.json(retorno);
      });
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

UsuarioController.prototype.remove = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  model.findOne(_id, function (err, data) {
    if (err) return next(err);
    if (!data) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    } else {
      var _id = data[0]._id;
      model.remove(_id, function (err, data) {
        if (err) return next(err);
        res.json('ok');
      });
    }
  });
}

UsuarioController.prototype.removeQuery = function (req, res, next) {
  var model = this.model;
  this.model.find(req.query, function (err, data) {
    if (err) return next(err);
    if (data.length) {
      for (let d in data) {
        model.remove(data[d]._id, function (err, data) {
          if (err) return next(err);
        });
        if (d == (data.length - 1))
          res.json('ok');
      }
    } else {
      res.json('ok')
    }
  });
}

UsuarioController.prototype.removeArray = function (req, res, next) {

  var model = this.model;

  var obj = req.body;

  var remove = function (obj) {
    if (!obj.length) {
      var err = new Error('Nenhum parâmetro para exclusão de objeto');
      err.status = 400;
      return next(err);
    }

    for (let i in obj) {
      model.findOne(obj[i], function (err, data) {
        if (err) return next(err);
        if (!data) {
          var err = new Error('Not Found');
          err.status = 404;
          return next(err);
        } else {
          var _id = data[0]._id;

          model.remove(_id, function (err, data) {
            if (err) return next(err);
            if (i == obj.length - 1)
              res.json('ok')
          });
        }
      });
    }
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      remove(obj);
    });
  } else {
    remove(obj);
  }


}

UsuarioController.prototype.aggregate = function (req, res, next) {
  this.model.aggregate(req.aggregate, function (err, data) {
    if (err) return next(err);
    res.json({
      data: data
    });
  });
};

UsuarioController.prototype.logIn = function (req, res, next) {
  let obj = req.body;
  let model = this.model;

  let logar = function (obj) {
    let encrypt = {
      login: md5(obj.login)
    }
    encrypt.password = md5(encrypt.login + obj.password);

    model.find({
      login: encrypt.login,
      password: encrypt.password
    }, function (err, data) {
      if (err) return next(err);
      if (data.length) {
        let user = data[0];
        var expires = moment().add(1, 'hour').valueOf();
        var token = jwt.encode({
          user: user.login,
          exp: expires
        }, config.get('jwtTokenSecret'));

        res.json({
          _id: user._id,
          nome: "",
          login: user.login,
          user_pic: "",
          modulosAutorizados: user._modulos.map(function (m) {
            return {
              _id: m._id,
              nome: m.nome,
              codigo: m.codigo,
              icone: m.icone,
              paginas: user._paginas.filter(function (p) {
                return p._modulo.toString() == m._id.toString();
              }).map(function (p) {
                return {
                  _id: p._id,
                  nome: p.nome,
                  codigo: p.codigo
                }
              })
            };
          }),
          token: token
        });
      } else {
        model.find({
          login: obj.login
        }, function (err, data) {
          if (err) return next(err);
          if (data.length) {
            var err = new Error('Senha incorreta. Tente novamente.');
            err.status = 401;
            next(err);
          } else {
            var err = new Error('O sistema não reconhece este usuário.');
            err.status = 401;
            next(err);
          }
        })
      }
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      logar(obj);
    });
  } else {
    logar(obj);
  }
};

module.exports = function (UsuarioModel) {
  return new UsuarioController(UsuarioModel);
};