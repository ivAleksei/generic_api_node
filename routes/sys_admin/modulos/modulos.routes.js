// modulos.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  nome: String,
  codigo: String,
  icone: String,
  ativo: Boolean
};

var Model = require('../../generic/generic.model')(mongoose, 'modulos', objSchema, '');
var Controller = require('../../generic/generic.controller')(Model);
console.log(Model.model.modelName);

router.get('/teste', function(req, res){
  res.json('teste');
})

router.use('/', require('../../generic/generic.js')(Controller));

module.exports = router;