'use strict'

var express = require('express');
var router = express.Router();

var fs = require('fs');
var path = require('path');

router.use('/modulos', require('./modulos/modulos.routes'));
// router.use('/paginas', require('./paginas/paginas.routes'));
// router.use('/usuarios', require('./usuarios/usuarios.routes'));


module.exports = router;