'use strict'

// PaginaController.js

var md5 = require('md5');
var fs = require('fs');
var path = require('path');

function PaginaController(PaginaModel) {
  this.model = PaginaModel;
}

PaginaController.prototype.getAll = function (req, res, next) {
  this.model.find({}, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

PaginaController.prototype.getActive = function (req, res, next) {
  this.model.find({
    ativo: true
  }, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

PaginaController.prototype.getByQuery = function (req, res, next) {
  this.model.find(req.query, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

PaginaController.prototype.getById = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  model.findOne(_id, function (err, data) {
    if (err) return next(err);

    if (!data.length) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    }

    res.json(data[0]);
  });

};

PaginaController.prototype.create = function (req, res, next) {

  var model = this.model;
  var obj = req.body;


  var persist = function (obj, files) {
    if (obj._id && obj._id != '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    if (obj._id == '')
      delete obj._id;

    model.create(obj, function (err, data) {
      if (err) {
        return next(err);
      }
      res.json(data);
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

PaginaController.prototype.createArray = function (req, res, next) {

  var model = this.model;
  var obj = req.body;


  var persist = function (obj, files) {
    if (!obj.length) {
      var err = new Error('Array nao informado.');
      err.status = 400;
      return next(err);
    }

    var retorno = [];
    for (let b in obj) {
      if (obj[b]._id && obj[b]._id != '') {
        var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
        err.status = 400;
        return next(err);
      }

      if (obj[b]._id == '')
        delete obj[b]._id;

      model.create(obj[b], function (err, data) {
        if (err) {
          return next(err);
        }
        retorno.push(data);
        if (b == obj.length - 1)
          res.json(retorno);
      });
    }
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

PaginaController.prototype.update = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  var obj = req.body;
  var persist = function (obj, files) {

    if (!_id || _id == '') {
      var err = new Error('Não foi possível atualizar. Identificador não informado.');
      err.status = 400;
      return next(err);
    }
    model.update(_id, obj, function (err, data) {
      if (err) return next(err);
      model.findOne(_id, function (err, data) {
        if (err) return next(err);
        let retorno = data[0];
        res.json(retorno);
      });
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

PaginaController.prototype.remove = function (req, res, next) {
  var _id = req.params._id;
  var model = this.model;

  model.findOne(_id, function (err, data) {
    if (err) return next(err);
    if (!data) {
      var err = new Error('Not Found');
      err.status = 404;
      return next(err);
    } else {
      var _id = data[0]._id;
      model.remove(_id, function (err, data) {
        if (err) return next(err);
        res.json('ok');
      });
    }
  });
}

PaginaController.prototype.removeQuery = function (req, res, next) {
  var model = this.model;
  this.model.find(req.query, function (err, data) {
    if (err) return next(err);
    if (data.length) {
      for (let d in data) {
        model.remove(data[d]._id, function (err, data) {
          if (err) return next(err);
        });
        if (d == (data.length - 1))
          res.json('ok');
      }
    } else {
      res.json('ok')
    }
  });
}

PaginaController.prototype.removeArray = function (req, res, next) {

  var model = this.model;

  var obj = req.body;

  var remove = function (obj) {
    if (!obj.length) {
      var err = new Error('Nenhum parâmetro para exclusão de objeto');
      err.status = 400;
      return next(err);
    }

    for (let i in obj) {
      model.findOne(obj[i], function (err, data) {
        if (err) return next(err);
        if (!data) {
          var err = new Error('Not Found');
          err.status = 404;
          return next(err);
        } else {
          var _id = data[0]._id;

          model.remove(_id, function (err, data) {
            if (err) return next(err);
            if (i == obj.length - 1)
              res.json('ok')
          });
        }
      });
    }
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      remove(obj);
    });
  } else {
    remove(obj);
  }


}

PaginaController.prototype.aggregate = function (req, res, next) {
  this.model.aggregate(req.aggregate, function (err, data) {
    if (err) return next(err);
    res.json({
      data: data
    });
  });
};


module.exports = function (PaginaModel) {
  return new PaginaController(PaginaModel);
};