// paginas.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  nome: String,
  codigo: String,
  ativo: Boolean,
  // _modulo: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'modulos'
  // },
};

var Model = require('../../generic/generic.model')(mongoose, 'paginas', objSchema, '_modulo');
var Controller = require('./paginas.controller')(Model);

router.use('/', require('../../generic/generic.js')(Controller));

module.exports = router;